<?php

/* Ecrire les m�thodes g�n�rant les headers et les footers */
class wbEtlParser extends wbEditGenerator
{
	private $m_etl_file = '';
	private $m_text = '';
	private $m_variables = Array();
	private $m_XML = NULL;
	
	private $m_EditGenerator = NULL;
	
	// ParseHeaderTag() ans ParseFooterTag() needs to return values 
	private $m_headerArguments = Array();
	private $m_footerArguments = Array();
	private $m_HeaderFunction = NULL;
	private $m_FooterFunction = NULL;
	
	public function __construct($a_etl_file, $a_binded_variables)
	{	
		$this->m_etl_file = $a_etl_file;
		$this->m_variables = $a_binded_variables;
		
		$this->m_XML = simplexml_load_file($this->m_etl_file);			
		
		$this->ParseHeaderTag();
		$this->ParseFooterTag();
		
		parent::__construct($this->m_HeaderFunction, $this->m_FooterFunction, 
											$this->m_headerArguments, $this->m_footerArguments
		);
	}	
	
	private function ParseVariables($a_text)
	{
		$l_offset = 0;
		
		while( $l_offset < strlen($a_text) and (($variable_name_opening_pos = strpos($a_text, '[', $l_offset)) !== false))
		{
			$l_variable_name = '';
			$i = $variable_name_opening_pos + 1;
			
			$str = substr($a_text, $variable_name_opening_pos);
			
			$pos = strpos($str, ']', 0);
			
			$l_variable_name = substr($str, 1, ($pos - 1));
			
			if(isset($this->m_variables[$l_variable_name]))
			{
				$a_text = str_replace("[$l_variable_name]", $this->m_variables[$l_variable_name], $a_text);
			}
			else
			{
				wbError::Raise("$l_variable_name n'existe pas dans le tableau des variables de l'objet");
			}
			
			$l_offset = $i;
		}
		
		return $a_text;
	}
	
	
	public function BindVariables(Array $a_variables)
	{
		$this->m_variables = $a_variables;
	}
	
	
	public function ToPDF()
	{
		if($this->m_XML == false)
		{
			echo "<pre>Impossible de charger " . $this->m_etl_file;
			
			foreach(libxml_get_errors() as $err)
				wbError::Raise($err->message);
				
			return false;
		} 
		else 
		{	
			$this->ParseETLFile($this->m_XML);
			$this->Output("lol.pdf",'I');
		}
	}
	
	
	private function ParseETLFile()
	{
		$l_titleAttributes = $this->m_XML->documentTitle->attributes();
		
		//$this->ParseDocumentTitleTag($this->m_XML);
		
		foreach($this->m_XML->box as $a_box)
			$this->ParseBoxTag($a_box);
	}
	
	
	private function ParseHeaderTag()
	{
		$l_logo_path = "";
		$l_document_title = "";
		$l_text_lines = Array();
			
		if(!empty($this->m_XML->header))
		{
			$l_header_tag = $this->m_XML->header;
			
			$l_document_title = $this->ParseTitleTag($l_header_tag);
			
			if(isset($l_header_tag->logo))
			{
				if(!empty($l_header_tag->logo->attributes())) 
					$l_logo_path = $l_header_tag->logo->attributes()->src->__toString();
				else
					$l_logo_path = "";
			}
			
			$l_text_lines = Array();
			foreach($l_header_tag->info as $info_tag)
			{
				array_push($l_text_lines, $info_tag[0]);
			}	

			/*if(!empty($l_header_tag->font))
				$this->ParseFontTag($l_header_tag->font);*/
			
		}
				
		$this->m_headerArguments["title"] = $l_document_title;
		$this->m_headerArguments["logo_path"] = $l_logo_path;
		$this->m_headerArguments["text_lines"] = $l_text_lines;
		
		$this->m_HeaderFunction = 
			function($a_object, $a_arguments) {				
				$l_logo_path = $a_arguments[ "logo_path"];
				$l_lines = $a_arguments["text_lines"];
				
				if(!empty($l_logo_path))
					$this->Image($l_logo_path, 10, 10, 50);
				
				$this->SetFont('Arial', 'I', 9);
				
				$l_text = "";
				$this->SetY(25);
				foreach($l_lines as $l_line)
				{
					$l_text_line = $this->ParseVariables(utf8_decode($l_line));
					$this->Cell(40,4, $l_text_line);
					$this->Ln();
				}
				
				//echo $l_text;
				
				$this->TextBox($l_text, 10, 10, 60, 10);
			}
		;
	}
	
	
	private function ParseFooterTag()
	{
		$l_notabene = "";
		$l_with_page_no = false;
		$l_page_no_header = "";
		
		
		if(!empty($this->m_XML->footer))
		{
			$l_footer_tag = $this->m_XML->footer;
			
			if(!empty($l_footer_tag->notaBene))
			{
				$l_notabene = $l_footer_tag->notaBene->__toString();
			}
				
			if(!empty($l_footer_tag->pageNo))
			{
				$l_with_page_no = true;
				$l_page_no_header = $l_footer_tag->pageNo->__toString();
			}
		}
		
		$this->m_footerArguments["notabene"] = $l_notabene;
		$this->m_footerArguments["with_page_no"] = $l_with_page_no;
		$this->m_footerArguments["page_no_header"] = $l_page_no_header;
		
		$this->m_FooterFunction = 
			function($object, $a_arguments)
			{				
				$l_notabene = $a_arguments["notabene"];
				$l_with_page_no = $a_arguments["with_page_no"];
				
				// Il faut parser la balise font ...
				$object->SetFont("Arial", 'I', 9);
				
				if(!empty($l_notabene))
				{
					$object->SetY(-20);
					$object->Cell(0,10, $l_notabene);
				}
				
				if($l_with_page_no)
				{
					$object->SetY(-15);
					$l_page_no_header = $a_arguments["page_no_header"];
					$object->Cell(0,10, $l_page_no_header . $this->PageNo() . '/{nb}', 0, 1, 'R');
				}
				
			}	
		;
	}
	
	
	private function ParseTitleTag($a_header_tag)
	{
		$l_document_title ="";
		$l_title_ordinate_from_top = 0;
		
		if(isset($a_header_tag->title))
		{
			$l_document_title = $a_header_tag->title;
			if(null !== $a_header_tag->title->attributes())
			{
				if(isset($a_header_tag->title->attributes()->ordinateFromTop))
					$l_title_ordinate_from_top = $a_header_tag->title->attributes()->ordinateFromTop;
			}
		}
		
		$this->SetDocTitle($l_document_title, $l_title_ordinate_from_top);
	}
		
		
	private function ParseFontTag($a_font_tag)
	{
		//wbDebug::Dump($a_font_tag);
		
		if(empty($a_font_tag->fontStyle))
			$l_font_style = "";
		else
			$l_font_style = $a_font_tag->fontStyle;
		
		if(!empty($a_font_tag->fontFamily) && !empty($a_font_tag->fontSize))			
			$this->SetFont($a_font_tag->fontFamily[0], $l_font_style, $a_font_tag->fontSize[0]);
	}
		
	private function ParseBoxTag($a_box)
	{
		if(!empty($a_box->boxXpos) && !empty($a_box->boxYpos))
		{
			$this->SetX($a_box->boxXpos[0]);
			$this->SetY($a_box->boxYpos[0]);
		}
	
		if(!empty($a_box->font))
			$this->ParseFontTag($a_box->font);
		
		$l_text_w = $this->GetStringWidth($a_box->text[0]);
		
		$a_boxAttributes  = $a_box->attributes();
		
		if(isset($a_boxAttributes['withBorders']))
		{
		  if($a_boxAttributes['withBorders'] == true)
		 {
			 //wbDebug::Dump($a_box->boxXpos);
			$this->Box($a_box->boxXpos[0], $a_box->boxYpos[0], 
								$a_box->boxWidth[0],  $a_box->boxHeight[0]);
		 }
		}
		
		//wbDebug::Dump($a_box);
		if(isset($a_box->text))
			$this->ParseTextTag($a_box);
	}
	
	
	private function ParseTextTag($a_box_tag)
	{
		$l_text_tag = $a_box_tag->text;
		$l_line_height = 5;
		
		if(null !== $l_text_tag->attributes())
		{
			$l_text_tag_attributes = $l_text_tag->attributes();
			
			if(isset($l_text_tag_attributes->lineHeight))
			{
				$l_line_height = $l_text_tag_attributes->lineHeight;
			}
			
			if(isset($l_text_tag_attributes->fromFile))
			{
				if(file_exists($l_text_tag_attributes->fromFile))
				{
					$l_text = file_get_contents($l_text_tag_attributes->fromFile);
				}
				else
				{
					wbError::Raise("Le chemin " . $l_text_tag_attributes['fromFile'] . " ne fait r�f�rence a aucun fichier");
				}
			}
			else
			{
				$l_text = utf8_decode($l_text_tag->__toString());
			}
		}
		else
		{
			$l_text = utf8_decode($l_text_tag->__toString());
		}
	
		$this->TextBox(utf8_encode($this->ParseVariables($l_text)), $a_box_tag->boxXpos[0] + 5, $a_box_tag->boxYpos[0] + 5, 
													$a_box_tag->boxWidth[0]-10, $l_line_height);
	}	
	
}

?>