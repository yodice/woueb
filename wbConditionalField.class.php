<?php

// Classe parente de tous les champs pouvant inclure une condition

class wbConditionalField extends wbReportField
{

const GreaterThan = ">";
const LesserThan = "<";
const GreaterOrEqualThan = ">=";
const LesserOrEqualThan = "<=";
const EqualThan = "=";
	
private $m_condition;
private $m_value;

public function __construct($a_fieldName, $a_condition, $a_value)
{
	parent::__construct($a_fieldName);
	
	$this->SetCondition($a_condition);
	$this->SetValue($a_value);
}

public function GetCondition()
{
	return $this->m_condition;
}

public function SetCondition($a_condition)
{
	$this->m_condition = $a_condition;
}

public function GetValue()
{
	return $this->m_value;
}

public function SetValue($a_value)
{
	$this->m_value = $a_value;
}

}

?>