<?php

class wbAlertType
{
	const Success = 1;
	const Danger = 2;
	const Info = 3;
	const Primary = 4;
}

?>