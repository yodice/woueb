<?php

// Rendre plus flexible la modification de chemin 
// avec des constantes ou des variables dans un fichier 
// ini ou autres ...

function LoadWouebClasses($a_class_name)
{
	$l_class_file_path = "";
	switch(strtolower($a_class_name))
	{
		case "tfpdf" :
			$l_class_file_path = "/var/www/woueb/tfpdf/tfpdf.php";
			break;
		
		case "cpdf" :
			$l_class_file_path = "/var/www/woueb/dompdf/lib/Cpdf.php";
			break;
		
		default:
			if(strtolower(substr($a_class_name, 0, 7)) == "dompdf\\")
				$l_class_file_path = "/var/www/woueb/dompdf/autoload.inc.php";	
			else if(substr($a_class_name,0,2) == "wb")
				$l_class_file_path = "/var/www/woueb/dev/" . "$a_class_name.class.php";
			else
				$l_class_file_path = _CLASS_DIR . $a_class_name . ".class.php";
	}
		
	require_once( $l_class_file_path );	
}

spl_autoload_register(
	function($a_class_name)
	{
		LoadWouebClasses($a_class_name);
	}
);

?>
