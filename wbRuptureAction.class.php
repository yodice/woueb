<?php

// Utilis�e comme une �num�ration

class wbRuptureAction
{

const Nothing = NULL;
const Sum = 1;
const Avg = 2;
const Count = 3;

public static function DoAction(wbRuptureAction $a_action, $a_Data)
{
	// A terme doit g�rer des noms de m�thodes dynamiques en fonction
	// des constantes de classe d�finies ci-haut.
	
	switch($a_action)
	{
		case wbRuptureAction::Sum :
			return wbRuptureAction::DoSum($a_Data);
		break;
		
		case wbRuptureAction::Count :
			return wbRuptureAction::DoCount($a_Data);
		break;
		
		case wbRuptureAction::Avg :
			return wbRuptureAction::DoAvg($a_Data);
		break;
	}
}

private static function DoSum($a_Data)
{	
	$sum = 0;
	
	foreach($a_Data as $num)
		$sum = $sum + $num;
		
	return $sum;
}

private static function DoCount($a_Data)
{
	return count($a_Data);
}

private static function DoAvg($a_Data)
{
	return wbRuptureAction::DoSum($a_Data) / wbRuptureAction::DoCount($a_Data);
}

}	

?>