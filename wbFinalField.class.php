<?php

// D�finit les champs qui ferment un tableau (somme ou moyenne de montants financiers ect...)

class wbRuptureField extends wbSelectField
{

private $m_method;

public function __construct($a_selected_field, $a_method_name,
							$a_width=10, $a_label="", $a_align="R")
{
	parent::__construct($a_selected_field, $a_width, $a_label, $a_align);
	
	$this->SetLabel("Total des " . $a_label);
	$this->SetMethod($a_method_name);
}

public function SetMethod($a_method_name)
{
	$this->m_method = $a_method_name;
}

public function GetMethod()
{
	return $this->m_method;
}

}

?>