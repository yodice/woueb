<?php

// D�finit les op�rations r�alis�es pour les sous-totaux
class wbSubTotalField extends wbSelectField
{

private $m_action;

public function __construct($a_selected_field, $a_rupture_action=wbRuptureAction::Nothing,
							$a_width=10, $a_label="", $a_align="R")
{
	parent::__construct($a_selected_field, $a_width, $a_label, $a_align);
	
	$this->SetAction($a_rupture_action);
}

public function SetAction($a_rupture_action)
{
	$this->m_action = $a_rupture_action;
}

public function GetAction()
{
	return $this->m_action;
}

public function GetValue()
{
}

private function SetValue()
{
}

}

?>