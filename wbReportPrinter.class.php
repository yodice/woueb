<?php

class wbReportPrinter extends tFPDF
{
	
const LITTLE_H = 7;
const MEDIUM_H = 10;

const BIG_P = 15;
const LITTLE_P = 7;

const XMED_SIZE = 80;
const BIG_SIZE = 120;
const XBIG_SIZE = 140;
const MEDIUM_SIZE = 20;
const LITTLE_SIZE = 10;

const LEFT_MARGIN = 7;

/* Text constants */

// A mettre autre part (� terme laisser le choix � l'utilisateur)
const _DATE_FORMAT = "d/m/Y";
const EDITION_DATE_TXT = "Edition du ";

private $name;
private $font;
private $header;
private $m_db;
	
public function __construct($name, $title, $font, $a_db, $margin)
{
	parent::__construct();
	
	$this->AddFont("DejaVu", "", "DejaVuSansCondensed.ttf", true);
	
	$this->date = date(self::_DATE_FORMAT);
	$this->m_db = $a_db;
	$this->name = $name;
	$this->title = $title;
	$this->font = $font;
	
	$this->SetLeftMargin(self::LEFT_MARGIN);
	$this->SetTopMargin($margin);
}
	
public function Header()
{
	// Logo
	if(file_exists(_LOGO_FILEPATH))
		$this->Image(_LOGO_FILEPATH, self::MEDIUM_H,8,33);
	
	// Title
    $this->SetFont($this->font,'B',self::BIG_P);
    $this->Cell(0, self::MEDIUM_H, $this->title, false, false, 'C');
	$this->Ln();
	
	// Put to right
	$this->Cell(self::XBIG_SIZE);
	
	// Edition date
	$this->setFont($this->font,'I',self::LITTLE_P);
	$this->Cell(0, self::LITTLE_H, self::EDITION_DATE_TXT . $this->date);
    $this->Ln(self::LITTLE_SIZE);
}

public function ToPDF(wbReportTable $a_reportTable)
{
	$l_Params = Array();
	
	$l_SQL = wbSQLMaker::MakeSQL($a_reportTable, $this->m_db);
	
	wbDebug::Dump($l_SQL);
	
	$a_reportTable->SetDataSourceFromStatement($this->m_db->query($l_SQL));
	
	$this->AddPage();
	
	$this->SimpleTable($a_reportTable);
}

private function SimpleTable(wbReportTable $reportTable)
{
	$this->SetFont("DejaVu",'',self::LITTLE_P);
	
	$this->SetDrawColor(200,200,200);
	$this->SetFillColor(232,232,255);
	
	$Select_fields = $reportTable->GetSelectFields();
	
	if($reportTable->IsThereAreRuptures())
	{
		$this->Cell(TFPDF::GetStringWidth("Sous-totaux") + 3, self::LITTLE_H,
						"", true, false,
						wbSelectField::AlignCenter, true);
	}
	
	// Affichage du label
	foreach($Select_fields as $Sfield)
	{	
		$this->Cell($Sfield->GetWidth(), self::LITTLE_H,
						$Sfield->GetLabel(), true, false,
						wbSelectField::AlignCenter, true);
	}
	
	$this->Ln();
	
	$this->SetDrawColor(232,232,232);
	
	$linecount = 1;
	while($reportTable->NextLine())
	{	
		
		if($reportTable->HasRupted())
		{			
			//wbDebug::Message("Rupture sur ligne $linecount");
			$this->SetDrawColor(120,120,120);
			$this->SetFillColor(230,230,230);
			
			$this->Cell(TFPDF::GetStringWidth("Sous-totaux") + 3, self::LITTLE_H,
							"Sous-totaux", true, false,
							wbSelectField::AlignCenter, true);
						
			while($reportTable->NextSubtotal())
			{
				//wbDebug::Dump($reportTable->GetCurrentSubtotalFieldValue());
				$this->Cell($reportTable->GetCurrentSubtotalFieldWidth(), self::LITTLE_H,
							$reportTable->GetCurrentSubtotalFieldValue(), true, false,
							$reportTable->GetCurrentSubtotalFieldAlign());
			}
			
			$this->Ln();
			
			$this->SetDrawColor(232,232,232);
		}
		
		$this->SetFillColor(232,232,255);

		// Si il y a un champ de totalisation on
		// ajoute du c�t� gauche de la ligne courante
		// un champ vide
		if($reportTable->IsThereAreRuptures())
		{
			$this->Cell(TFPDF::GetStringWidth("Sous-totaux") + 3, self::LITTLE_H,
							"", true, false,
							wbSelectField::AlignCenter, true);
		}
		
		
		// Affichage de tous les champs
		while($reportTable->NextField())
		{	
			//wbDebug::Message($reportTable->GetCurrentWidth());
			$this->Cell($reportTable->GetCurrentWidth(), self::LITTLE_H,
						utf8_encode($reportTable->GetCurrentValue()), true, false,
						$reportTable->GetCurrentAlign());
		}
			
		$this->Ln();
		
		$linecount++;
	}
}

public function Footer()
{
    $this->SetY( -(self::MEDIUM_H) );
    $this->SetFont($this->font,'I', self::LITTLE_P);
    $this->Cell(0,self::MEDIUM_H, "Page ".$this->PageNo()." / {nb}",0,0,'R');
}

public function LoadData($file)
{
	//Read file lines
	$lines=file($file);
	$data=array();
	foreach($lines as $line)
		$data[]=explode(';',chop($line));
	return $data;
}

public function SetHeader($header)
{
	$this->header = $header;
}
 
public function ClosePDF() 
{
	$this->AliasNbPages();	
	$this->Close();
}
	
}

?>