<?php

class wbEditGenerator extends tFPDF
{
	
private $m_title = "TITLE";
private $m_title_ypos = 0;
private $m_nota_bene = NULL;

private $m_society_info; // Array
private $m_logo_path;


// Fonctions param�trant les m�thodes d'affichage
private $HeaderMethod = NULL;
private $FooterMethod  = NULL;
private $m_headerArguments = Array();
private $m_footerArguments  = Array();

public function __construct($a_header_function = NULL, $a_footer_function = NULL, $a_headerArguments = Array(), $a_footerArguments = Array())
{
	if(is_array($a_headerArguments))
		$this->m_headerArguments = $a_headerArguments;
	
	if(is_array($a_footerArguments))
		$this->m_footerArguments = $a_footerArguments;
	
	if(isset($a_headerArguments[ "text_lines " ]))
		$this->m_society_info = $a_headerArguments[ "text_lines " ];
	
	if(isset($a_headerArguments[ "logo_path" ]))
		$this->m_logo_path = $a_headerArguments[ "logo_path" ];
	
	if(isset($a_footerArguments[ "notabene" ]))
		$this->m_nota_bene = $a_footerArguments[ "notabene" ];
	
	if(is_callable($a_header_function))
	{
		$this->HeaderMethod = $a_header_function;
	}
	
	if(is_callable($a_footer_function))
	{
		$this->FooterMethod = $a_footer_function;
	}

	parent::__construct();
	$this->AddPage();
	$this->AliasNbPages();
}

public function PrintSocietyInfo()
{
	$ypos = 20;
	$this->SetFont("Arial",'I',9);
	
	if(!empty($this->m_society_info))
	{
		foreach($this->m_society_info as $l_info)
		{
			$ypos += 4;
			$this->SetY($ypos);
			$this->Cell(30, 0, utf8_decode($l_info), 0, 1, 'L');
		}
	}
}

function Header()
{
	//wbDebug::Dump($this->HeaderMethod);
	if(is_callable($this->HeaderMethod))
	{
		$header = $this->HeaderMethod;
		$header($this, $this->m_headerArguments);
	}
	
	$this->PrintTitle();
}

private function PrintTitle()
{
	// Arial gras 22
	$this->SetFont('Arial','B',22);
	// Calcul de la largeur du titre et positionnement
	$w = $this->GetStringWidth($this->m_title)+20;
	$this->SetY($this->m_title_ypos);
	$this->SetX((210-$w)/2);
	// Couleurs du cadre, du fond et du texte
	$this->SetFillColor(255,255,255);
	// Epaisseur du cadre (1 mm)
	$this->SetLineWidth(0.1);
	// Titre
	$this->Cell($w,20,$this->m_title,1,1,'C',true);
	// Saut de ligne
	$this->Ln(20);
}

public function Body()
{
	$this->AddPage();
}

function Footer()
{
	if(is_callable($this->FooterMethod))
	{
		$footer = $this->FooterMethod;
		$footer($this, $this->m_footerArguments);
	}
	/*if($this->m_nota_bene != NULL)
	{
		// Positionnement � 1,5 cm du bas
		$this->SetY(-20);
		// Arial italique 8
		$this->SetFont('Arial','I',9);
		// Couleur du texte en gris
		$this->SetTextColor(128);
		// Num�ro de page
		$this->Cell(0,10,utf8_decode("NB: " . $this->m_nota_bene));
		$this->Ln();
	}*/
}

public function SetDocTitle($a_title, $a_ypos = 60)
{
	$this->m_title = utf8_decode($a_title);
	$this->m_title_ypos = $a_ypos;
}

public function AddTextBox($a_text, $a_xpos, $a_ypos, $a_width = 0, $a_height = 5, $a_with_borders = false)
{
	
	if($a_with_borders)
	{
		$l_text_width = $this->GetStringWidth($a_text);
		$this->Box($a_xpos, $a_ypos, $l_text_width + 10, $a_height);
	}
	
	$this->AddText($a_text, $a_xpos, $a_ypos, $a_width, $a_height);
}

public function Box($a_xpos, $a_ypos, $a_width, $a_height)
{
	$l_lastXpos = $this->GetX(); $l_lastYpos = $this->GetY();
	$this->SetY($a_ypos); $this->SetX($a_xpos);
 	$this->Cell($a_width, $a_height,"",1);
	$this->SetY($l_lastXpos); $this->SetX($l_lastYpos); 
}

// Positionne un texte apr�s la derni�re position en ordonn�e et modifie la police avant �criture
public function AddText($a_text, $a_xpos = 30, $a_blank = 10, $a_width = 0, $a_height = 5,
										$a_font_family = "Helvetica", $a_font_style = '', $a_font_size = 9)
{
	$a_text = utf8_decode($a_text);
	
	$l_lastXpos = $this->GetX(); $l_lastYpos = $this->GetY();
	$this->SetFont($a_font_family, $a_font_style, $a_font_size);
	$this->TextBox($a_text, $a_xpos, $a_ypos + $this->GetY(), $a_width, $a_height);
	/*$this->SetFillColor(255,255,255);
	// Sortie du texte justifi�
	$this->SetY($this->GetY() + $a_blank);
	$this->SetX($a_xpos);
    $this->MultiCell($a_width, $a_height ,$a_text);
	$this->Ln();
	$this->SetX($l_lastXpos); $this->SetY($l_lastYpos);*/
}

// Position un texte en ordonn�e $a_ypos mais ne modifie pas le police avant �criture
public function TextBox($a_text, $a_xpos = 30, $a_ypos = 10, $a_width = 0, $a_height = 5)
{
	$a_text = utf8_decode($a_text);
	
	$l_lastXpos = $this->GetX(); $l_lastYpos = $this->GetY();
	$this->SetFillColor(255,255,255);
	
	$this->SetXY($a_xpos, $a_ypos);
    $this->MultiCell($a_width, $a_height ,$a_text);
	$this->SetXY($l_lastXpos, $l_lastYpos);
	$this->Ln();
}

public function SetNotaBene($a_nota_bene)
{
	$this->m_nota_bene = $a_nota_bene;
}

public function SetSocietyInfo($a_society_info)
{
	$this->m_society_info = $a_society_info;
}

public function SetLogoPath($a_logo_path)
{
	$this->m_logo_path = $a_logo_path;
}

}

?>