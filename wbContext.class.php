<?php

require_once "woueb.php";

class wbContext
{
	
private $m_INIConsts;
private $m_INIFilePath;
	
public function __construct($a_INIFilePath)
{
	$this->m_INIFilePath = $a_INIFilePath;
	$this->LoadIniConsts();
}

public function LoadIniConsts()
{
	$this->m_INIConsts = parse_ini_file($this->m_INIFilePath, false, INI_SCANNER_TYPED);
}

// D�finit en constantes toutes la constante dans le .ini. dont 
// le nom est pass� en argument.
// Le nom de la constante est capitalis� et _INI_ � chaque nom
// est ajout�
public function DefineIniConst($const_name)
{
	define('_INI_' . strtoupper($const_name), $this->GetConstVal($const_name));
}

public function GetConstVal($const_name)
{
	$const_name = strtolower($const_name);
	
	if(empty(
	($const_val = $this->m_INIConsts[ $const_name ])))
		wbError::Raise("wbContext", "GetConstVal", "The constant $const_val requested does not exist yet");

	return $const_val;
}

// N�cessaire pour les inclusions de fichier de la librairie
// A impl�menter dans une classe fille
private function GenerateVitalConsts()
{
	define("WOUEB_PATH", $this->m_INIConsts[ "root_path" ] . $this->m_INIConsts[ "version" ] . '/');
	define("TFPDF_PATH", _INI_ROOT_PATH . "tfpdf/");
	define("WB_CLASSES_PATH", WOUEB_PATH . _INI_CLASS_DIR);	
}

public function GetIniConsts()
{
	return $this->m_INIConsts;
}

}