<?php

class wbData
{
	
public static function RemoveIntegerKeys(Array &$A)
{
	$AKeys = array_keys($A);
	
	for($i=0; $i < count($AKeys); $i++)
	{
		$key = $AKeys[ $i ];
		
		if(is_int($key))
			unset($A[ $key ]);
	}
}
	
}