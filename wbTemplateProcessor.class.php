<?php

class wbTemplateProcessor extends XMLReader
{
	public function __construct($a_xml_filename)
	{
		if(!$this->open($a_xml_filename))
			wbError::Raise("Fichier XML non existant : " . $a_xml_filename);//parent::__construct();
			
		$this->setParserProperty(XMLReader::VALIDATE, true);

		if(!$this->isValid())
			wbError::Raise("Fichier XML non valide : " . $a_xml_filename);

		$this->next();

		if(!$this->read())
			wbError::Raise("Impossible de lire le fichier XML : " . $a_xml_filename);
	}
}

?>