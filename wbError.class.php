<?php

// Appelez les méthodes de wbError de façon statique
class wbError
{
	const ERRM_UNDEFINED = "Undefined behavior";
	
	public static function Raise($object="", $method="", $errorStr = UNDEFINED )
	{
		$message = wbError::MakeErrMsg($object, $method, $errorStr);
		echo '<div class="alert a-is-danger fade in">';
		echo 	'<pre>' . $message . '</pre>';
		echo '</div>';
	}

	public static function RaiseFatal($object="", $method="", $errorStr = UNDEFINED )
	{
		wbError::Raise($object, $method, $errorStr);
		die("<pre>Previous error was fatal : execution <b>stopped</b></pre>");
	}
	
	private static function MakeErrMsg($object="", $method="", $errorStr)
	{
		$message = "Error in <b>";
		
		if( !empty($object))
			$message .= "$object::";
			
		$message .= "$method()</b> : $errorStr";

		return $message;
	}
}