<?php

require_once "woueb.php";

class wbDebug
{
	
const FORMAT_WITH_SCHEMA = true;
const DEBUG_ON = true;								// Laisser le choix de la valeur de cette constante dans un .INI
const DEBUG_PROMPT = "<b>[!!] <u>DEBUG</u></b> : ";
	
public static function Stop($class_name = "Unspec", $method_name = "Unspec")
{
	echo '<div class="alert a-is-info">';
	die(self::DEBUG_PROMPT . "Debug stop in $class_name::$method_name()</pre>");
}

public static function Dump($var, $class_name=NULL, $method_name=NULL, $explanation=NULL)
{
	if(wbDebug::DEBUG_ON)
	{			
		echo '<div class="alert a-is-info">';
		echo 	"<pre>" . self::DEBUG_PROMPT;
		
		if( !empty($class_name) )
		 
			echo " $class_name";
		if( !empty($method_name) )
			echo "::$method_name()";

		if(!empty($class_name) || !empty($method_name))
			echo '<br/><b>value dumped = </b>';
		
		echo '<span class="fa fa-info">';
		  print_r($var);
		echo "</span>";
		
		if(!empty($explanation))
			echo "<i>$explanation</i>";
		
		echo 	"</pre>";
		echo '</div>';
	}
}

/*********************************************** /
 * Eviter redondance entre Dump() et Message() *
 ***********************************************/
 
public static function Message($str_message, $class_name=NULL, $method_name=NULL, $typeOfAlert=wbAlertType::Success)
{
	if(wbDebug::DEBUG_ON)
	{		

		// A séparer et adapter à bootstrap
		switch($typeOfAlert)
		{
			case wbAlertType::Success :
				$alertType = "a-is-info";
			break;
			
			case wbAlertType::Danger :
				$alertType = "a-is-danger";
			break;
		
			case wbAlertType::Primary :
				$alertType = "a-is-primary";
			break;
		
			default :
				$alertType = "a-is-info";
			break;
		}
		
		echo '<div class="alert ' . $alertType . '">';
		echo 	"<pre>" . self::DEBUG_PROMPT;
		
		if( !empty($class_name) )
		 
			echo " $class_name";
		if( !empty($method_name) )
			echo "::$method_name()";

		echo "<i>$str_message</i>";
		
		echo 	"</pre>";
		echo '</div>';
	}
	
}

}