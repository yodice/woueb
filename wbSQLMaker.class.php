<?php

class wbSQLMaker
{
	
// Génère la requête SQL en fonction des données du report voulue
public static function MakeSQL(wbReportTable $a_reportTable, wbMySQL $a_db, $a_isMakingEnd=false)
{	
	$l_Final_fields = $a_reportTable->GetFinalFields();
	$l_Select_fields = $a_reportTable->GetSelectFields();
	$l_Where_fields = $a_reportTable->GetWhereFields();
	$l_GroupBy_fields = $a_reportTable->GetGroupByFields();
	$l_Tables = $a_reportTable->GetSQLTables();
	
	$l_SQL = "";
	$l_where_SQL = "";
	$l_groupBy_SQL = "";
	$l_select_SQL = "";
	
	if($a_isMakingEnd)
	{
		$l_SQL = wbSQLMaker::CreateFinalSelectClause($l_Final_fields, $l_Tables);
	}
	else 
	{
		if(!empty($l_Select_fields))
			$l_select_SQL = wbSQLMaker::CreateSelectClause($l_Select_fields, $l_Tables);
		
		$l_SQL .= $l_select_SQL;
	}
	
	if(!empty($l_Where_fields))
		$l_where_SQL = wbSQLMaker::CreateWhereClause($l_Where_fields);

	if(!empty($l_GroupBy_fields))
		$l_groupBy_SQL = wbSQLMaker::CreateGroupByClause($l_GroupBy_fields);
		
	$l_SQL = $l_SQL . $l_where_SQL; //$groupBy_SQL . $having_SQL;
	
	if($a_reportTable->IsThereAreRuptures())
	{
		// Ordonne les champs pour que les ruptures soient effectives
		$l_rupture_restriction_SQL = wbSQLMaker::CreateOrderByClause($a_reportTable->GetRuptures());
		$l_SQL = $l_SQL . $l_rupture_restriction_SQL;
	}
	
	return $l_SQL; //. $groupBy_SQL . $having_SQL;
}
	
private static function AppendCommaOrSpace($SQL, $T, $tcount)
{
	if($tcount < count($T) - 1)
		$SQL .= ',';
	else
		$SQL .= ' ';
	
	return $SQL;
}
	
private static function CreateSelectClause(Array $a_Sfields, Array $a_SQLTables)
{	
	$l_SQL = "SELECT ";
	
	
	for($l_scount=0; $l_scount < count($a_Sfields); $l_scount++)
	{
		$l_f = $a_Sfields[ $l_scount ];
	
		$l_SQL .= '`' . $l_f->GetFieldName() . '`';
	
		$l_SQL = wbSQLMaker::AppendCommaOrSpace($l_SQL, $a_Sfields, $l_scount);
	}
	
	$l_SQL .= wbSQLMaker::AddTables($a_SQLTables);
	
	return $l_SQL;
} 

private static function CreateFinalSelectClause(Array $FSfields, Array $SQLTables)
{
	$SQL = "SELECT ";
	
	for($fscount=0; $fscount < count($FSfields); $fscount++)
	{
		$f = $FSfields[ $fscount ];
		
		$SQL .= $f->GetMethod() . '(`' . $f->GetFieldName() . '`)';
	}
	
	$SQL .= ' ' . wbSQLMaker::AddTables($SQLTables);
	
	return $SQL;
}

private static function AddTables(Array $SQLTables)
{
	$SQL = "FROM ";
	
	for($tcount=0; $tcount < count($SQLTables); $tcount++)
	{
		$SQL .= '`' . $SQLTables[ $tcount ] . '`';
		$SQL = wbSQLMaker::AppendCommaOrSpace($SQL, $SQLTables, $tcount);
	}
	
	return $SQL;
}


private static function CreateGroupByClause(Array $Gbfields)
{
	$SQL = "GROUP BY ";
	
	for($gbcount=0; $gbcount < count($Gbfields); $gbcount++)
	{
		$f .= $Gbfields->GetFieldName();
		
		wbSQLMaker::AppendCommaOrSpace($SQL, $Gbfields, $gbcount);
	}
	
	// Why does I don't have an automatic space ??
	return $SQL . " ";
}

private static function CreateSortClause(Array $Ofields)
{
	$SQL = "ORDER BY ";
	
	foreach($F as $fieldName => $sense)
	{
		$SQL .= $fieldName . " ";
		
		if($sense)
			$SQL .= $sense . " ";
	}
	
	return $SQL;
}

private static function CreateWhereClause(Array $Wfields)
{	
	$SQL = "WHERE ";
	
	for($fcount=0; $fcount < count($Wfields); $fcount++)
	{
		$f = $Wfields[ $fcount ];
		
		
		$SQL = $SQL . $f->GetFieldName() . " " . $f->GetCondition() . " '" . $f->GetValue() . "'";		
		
		//wbSQLMaker::EndClause()
		if($fcount < count($Wfields) - 1)
		{
			$SQL .= " AND ";
		}
		else
		{
			$SQL .= " ";
		}
	}
	
	return $SQL;
}

private static function CreateOrderByClause(Array $OBfields)
{
	$SQL = "ORDER BY ";
	
	$obcount = 0;
	foreach($OBfields as $fieldName => $FieldInf)
	{
		$SQL = $SQL . $fieldName . " ASC";
	
		$SQL = wbSQLMaker::AppendCommaOrSpace($SQL, $OBfields, $obcount);
		$obcount = $obcount + 1;
	}
	
	return $SQL;
}

private static function CreateHavingClause(Array $F, PDO $db)
{
	$SQL = "HAVING ";
	
	for($fcount=0; $fcount < count($F); $fcount++)
	{
		foreach($F as $k => $Lim)
		{
			$SQL .= $k . " " . $Lim[ 0 ] . " '" . $Lim[ 1 ] . "'";
		}
	}	
	
	return $SQL;
}	

}
?>