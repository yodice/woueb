<?php

class wbReportField
{

const TypeInt = "int";
const TypeStr = "str";
const TypeDouble = "double";
	
private $m_type;
private $m_fieldName;

public function __construct($a_fieldName, $a_type=wbReportField::TypeStr)
{
	$this->SetFieldName($a_fieldName);
	$this->SetType($a_type);
}

public function SetType($type)
{
	$this->m_type = $type;
}

public function GetType()
{
	return $this->m_type;
}

public function SetFieldName($a_fieldName)
{
	$this->m_fieldName = $a_fieldName;
}

public function GetFieldName()
{
	return $this->m_fieldName;
}

public function __toString()
{
	return $this->GetFieldName();
}

}

?>