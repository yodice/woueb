<?php

class wbSelectField extends wbReportField
{

const AlignRight = "R";
const AlignCenter = "C";
const AlignLeft = "L";

  /*****************/
  /* Field display */
  /*****************/
  
private $m_label;
private $m_align;
private $m_width;

  /*****************/
  /* Number format */
  /*****************/
  
private $m_num_format_str;
private $m_thousand_separator;
private $m_decimal_point;
private $m_decimal_precision;

public function __construct($a_fieldName, $a_width=10,
							$a_label="", $a_align="L",
							$a_type=wbReportField::TypeStr,
							$a_num_format_str='en')
{
	parent::__construct($a_fieldName, $a_type);
	
	$this->SetWidth($a_width);
	$this->SetLabel($a_label);
	$this->SetAlign($a_align);
	
	$this->SetType($a_type);
	$this->SetNumFormat($a_num_format_str);
}

public function SetNumFormat($a_num_format_str)
{	
	$this->m_num_format_str = $a_num_format_str;
}

public function GetNumFormat()
{
	return $this->m_num_format_str;
}

private function SetDecimalPoint($a_decimal_point)
{
	$this->m_decimal_point = $a_decimal_point;
}

public function GetDecimalPoint()
{
	return $this->m_decimal_point;
}

private function SetThousandSeparator($a_separator)
{
	$this->m_thousand_separator = $a_sep;
}

public function GetThousandSeparator()
{
	return $this->m_thousand_separator;
}

public function SetWidth($a_width)
{
	$this->m_width = $a_width;
}

public function GetWidth()
{
	return $this->m_width;
}

public function SetLabel($a_label)
{
	$this->m_label = $a_label;
}

public function GetLabel()
{
	if(empty($this->m_label))
		return $this->GetFieldName();
	else
		return $this->m_label;
}

public function SetAlign($a_align)
{
	$this->m_align = $a_align;
}

public function GetAlign()
{
	return $this->m_align;
}

public function SetType($type)
{
	parent::SetType($type);
	
	$_m_type = $this->GetType();
	
	if($_m_type == wbReportField::TypeInt
		|| $_m_type == wbReportField::TypeDouble)
	{
		$this->SetDecimalPrecision(2);
	}
}

public function SetDecimalPrecision($a_decimal_precision)
{
	if(is_int($a_decimal_precision))
		$this->m_decimal_precision = $a_decimal_precision;
}

public function GetDecimalPrecision()
{
	return $this->m_decimal_precision;
}

}
?>