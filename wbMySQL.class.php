<?php

/*
  CLASS wbMYSQL (woueb)
  Contributors :
  - PREVOST Corentin - cocauw@gmail.com
*/
 
class wbMySQL extends PDO 
{
	const Charset = "utf8";
	private $m_charset = wbMySQL::Charset;
	private $m_connection_string = "";
	private $m_host = null;
	private $m_db_name = null;

	private static $ErrBadArrayFormat = 
		'Param must be Array(:label => Array(paramName => PDO::PARAM_*))';

	public function __construct( $dbname, $host, $usrn, $pwd="" )
	{	
		try {
			parent::__construct("mysql:host=$host;dbname=$dbname",$usrn,$pwd);
		} catch (PDOException $e) {
			wbError::RaiseFatal("wbMySQL" ,"__construct", $e->getMessage() . "<br/>");
		}
	}

	/** \brief Send a SQL query and bind parameters.
	 * Returns an array of the result. PDO details are transparent. 
	 */
	public function SendQuery($a_sql, $a_Params = NULL, $a_withLabels = false)
	{
		try {
			$loc_stmt = $this->prepare($a_sql);
			
			if (!empty($a_Params))
			{
				if($a_withLabels)
					$loc_stmt = $this->BindLblParams($loc_stmt, $a_Params);
				else
					$loc_stmt = $this->BindParams($loc_stmt, $a_Params);
			}
			
			$loc_stmt->execute();
			
			if(!$loc_stmt)
				Raise("Requête non exécutée : $sql");
			else
			{
				$loc_req_result = $loc_stmt->fetchAll();

				if(count($loc_req_result) > 1)
					return $loc_req_result;	
				else
					return $loc_req_result[0];	
			}
				
		} catch(PDOException $e)
		{
			wbError::RaiseFatal("wbMySQL", "send_query", "<b>PDO Error</b>: " . $e->getMessage() . "<br/>");
		}
		catch(Exception $e)
		{
			wbError::RaiseFatal("wbMySQL", "send_query", $e->getMessage() . "<br/>");
		}
	}

	private function BindLblParams(PDOStatement $a_stmt, Array $a_params)
	{	
		foreach($a_params as $lp_label => $lp_param)
		{
			if(count($lp_param) != 2)
				throw new Exception(wbMySQL::ErrBadArrayFormat);
				
			if($lp_label[ 0 ] != ':')
				$lp_label = ':' . $lp_label;
				
			$a_stmt->bindValue($lp_label, $lp_param[ 0 ], $lp_param[ 1 ]);
		}

		return $a_stmt;
	}
	
	private function BindParams(PDOStatement $a_stmt, Array $a_params)
	{			
		foreach($a_params as $content => $type)
			$stmt->bindValue($label, $content, $type);	

		return $stmt;
	}

}

?>
