<?php

class wbDataFormatter
{
	
const FrNumFormat = "fr";
const UsNumFormat = "us";
const EnNumFormat = "en";

public static function FormatNumber($a_number, $format_str)
{
	$number = number_format($a_number, 2,
										wbDataFormatter::DecimalPoint($format_str),
										wbDataFormatter::ThousandSeparator($format_str));
	
	return $number;
}

public static function DecimalPoint($a_num_format_str)
{
	switch($a_num_format_str)
	{
		case wbDataFormatter::FrNumFormat:
			$decimal_point = ',';
		break;
		
		case wbDataFormatter::EnNumFormat:
		case wbDataFormatter::EnNumFormat:
			$decimal_point = '.';
		break;
		
		default:
			wbError::Raise("wbDataFormatter", "DecimalPoint", "$a_num_format_str est un mauvais format de nombre");
		break;
	}
	
}

public static function ThousandSeparator($a_num_format_str)
{
	switch($a_num_format_str)
	{
		case wbDataFormatter::FrNumFormat:
			$thousand_separator = ' ';
		break;
		
		case wbDateFormatter::EnNumFormat:
		case wbSelectField::EnNumFormat:
			$thousand_separator = ',';
		break;
		
		default:
			wbError::Raise("wbDataFormatter", "ThousandSeparator", "$a_num_format_str est un mauvais format de nombre");
		break;
	}
}

}

?>