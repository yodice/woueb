<?php

class wbReportTable
{

private $m_tables = Array();
private $m_select_fields = Array();
private $m_where_fields = Array();
private $m_groupBy_fields = Array();
private $m_end_fields = Array();
private $m_subtotal_fields = Array();

private $m_data_source = Array();
private $m_data_line_count = 0;
private $m_data_line_counter = 0;

private $m_current_data_line  = Array();

private $m_current_field = NULL;
private $m_current_field_name = "";
private $m_current_field_value = NULL;
private $m_current_field_label = "";
private $m_current_field_width = 0;
private $m_current_field_counter = 0;
private $m_current_field_align = "C";
private $m_current_fields_count = 0;

private $m_ruptures_on_fields = Array();
private $m_has_rupted = Array(); 			// Informe sur quel champ a eu lieu la rupture	

private $m_last_rupture_value = NULL;
private $m_current_rupture_value = NULL;

private $m_subtotals_fields_count = 0;
private $m_subtotals_fields_counter = 0;

private $m_current_subtotal_field_align = "C";
private $m_current_subtotal_field_width = 0;
private $m_current_subtotal_field_value = Array();

private $m_current_subtotals_acc = Array();

private $m_end_request = "";
private $m_end_data = Array();

public function __construct()
{
}

public function AddSubTotalField(wbSubTotalField $a_subtotal_field)
{
	try
	{	
		$SubtotalFieldName = $a_subtotal_field->GetFieldName();
		
		if( !empty($SubtotalFieldName) )
		{
			$this->m_current_subtotals_acc[ $SubtotalFieldName ] = 0;
			array_push($this->m_subtotal_fields, $a_subtotal_field);
			$this->m_subtotals_fields_count = $this->m_subtotals_fields_count + 1;
		}
		else
			throw new Exception("Le champ $a_subtotal_field n'est pas sélectionné");
	}
	catch(Exception $e)
	{
		wbError::Raise("wbReportTable", "AddSubTotalField", $e->getMessage());
	}
}

public function GetRupture($a_subtotal_field_name)
{
	try
	{
		if(empty($this->m_ruptures[ $a_subtotal_field_name ]))
		// POURQUOI CA MARCHE PAS ????
		//if( !($this->IsRuptureExisting($a_subtotal_field_name)) )
			throw new Exception("La rupture sur le champ $a_subtotal_field_name n'existe pas");
		
		return $this->m_ruptures[ $a_subtotal_field_name ];
	}
	catch(Exception $e)
	{
		wbError::Raise("wbReportTable", "GetRupture", $e->getMessage());
		return false;
	}
}

public function IsThereAreRuptures()
{
	if( !empty($this->m_ruptures_on_fields) )
		return true;
	else
		return false;
}

public function SetRuptureOn($a_subtotal_field_name)
{	
	try {
		
		if( !($this->IsARuptureExisting($a_subtotal_field_name)) )
		{
			$this->m_ruptures_on_fields[ $a_subtotal_field_name ] = True;	
			$this->m_has_rupted[ $a_subtotal_field_name ] = false;
		}
		else
			throw new Exception("Impossible d'ajouter une rupture déjà existante");
	} 
	catch(Exception $e)
	{
		wbError::Raise("wbReportTable", "SetRuptureOn", $e->getMessage());
		return false;
	}
}

// semble toujours renvoyer false ...
private function IsARuptureExisting($a_field_name)
{	
	if( empty($this->m_rupture_on_fields[ $a_field_name ]) )
		return false;
	else
		return true;
}

public function SetRuptureAction($a_subtotal_field_name,
									wbRuptureAction $a_rupture_action)
{
	try {
		if($this->IsARuptureExisting($a_subtotal_field_name))
			$this->m_ruptures_on_fields[ $a_subtotal_field_name ] = $a_rupture_action;
		else
			throw new Exception("La rupture $a_subtotal_field_name n'existe pas dans ce tableau de report.");
	}
	catch(Exception $e)
	{
		wbError::Raise("wbReportTable", "SetRuptureAction", $e->getMessage());
		return false;
	}
}


public function GetCurrentSubTotalFieldWidth()
{
	return $this->m_current_subtotal_field_width;
}

public function GetCurrentSubTotalFieldAlign()
{
	return $this->m_current_subtotal_field_align;
}

private function GetCurrentSubTotalField()
{
	return $this->m_subtotal_fields[ $this->m_subtotals_fields_counter ];
}

public function NextSubtotal()
{	
	if($this->m_subtotals_fields_counter >= $this->m_subtotals_fields_count)
		return false;
	
	wbDebug::Message("Va au sous-total suivant");
	
	$SubTotalField = $this->GetCurrentSubTotalField();
	
	//$this->m_current_subtotal_field_name = $SubTotalField->GetFieldName();
	$this->m_current_subtotal_field_width = $SubTotalField->GetWidth();
		
	$this->m_current_subtotal_field_value = $SubTotalField->GetValue(); 	// Doit pouvoir effectuer l'ensemble
																		// des calculs sur les données entre
																		// la rupture présente et précédente
	$this->m_current_subtotal_field_align = $SubTotalField->GetAlign();
	
	$this->m_subtotals_fields_counter = $this->m_subtotals_fields_counter + 1;
	
	return true;
}

public function BeginSubtotal()
{
	$this->m_subtotals_fields_counter = 0;
}

private function SetSubTotals()
{
	$CurrentDataLine = $this->GetCurrentDataLine();

	foreach($this->m_subtotal_fields as $SubtotalField)
	{
		$subtotalFieldName = $SubtotalField->GetFieldName();
		
		// if($this->HasRupted())
		// {
			// $this->m_current_subtotals_acc[ $subtotalFieldName ] = 0;
		// }
		// else
		// {
			$Field = $this->GetSelectFieldByName($subtotalFieldName);
			$fieldName = $Field->GetFieldName();
								
			if( !empty($fieldName) )
				$this->m_current_subtotals_acc[ $fieldName ] = $this->m_current_subtotals_acc[ $fieldName ] + $CurrentDataLine[ $fieldName ];
		// }
	}
}

private function HasSubTotal($a_field_name)
{
	if( !empty($this->m_subtotal_fields[ $a_field_name]) )
		return true;
	else
		return false;
}

public function NextLine()
{	
	if($this->m_data_line_counter >= $this->m_data_line_count - 1)
		return false;
	
	$this->m_current_field_counter = 0;
	$this->m_current_data_line = $this->GetDataLine($this->m_data_line_counter++);

	if($this->HasRupted())
		$this->SetSubTotals();
	
	$this->SetRuptureLine();
	
	return true;
}

public function PreviousLine()
{
	if($this->m_current_field_counter < 1)
		return false;
	
	$this->SetRuptureLine();
	
	$this->m_current_field_counter = 0;
	$this->m_current_data_line = $this->GetDataLine($this->m_data_line_counter--);
	wbData::RemoveIntegerKeys($this->m_current_data_line);
	return true;
}

public function BeginField()
{
	$this->m_current_field_counter = 0;
}

private function SetRuptureLine()
{
	$this->BeginField();
	
	while($this->NextField())
	{
		//wbDebug::Message("Testing rupture on " . $this->GetCurrentFieldName());
		if( $this->IsFieldHavingRupture($this->GetCurrentFieldName()) )
			$this->SetRuptureInfo();
	}
	
	$this->BeginField();
}

public function NextField()
{
	if($this->m_current_field_counter >= $this->m_current_fields_count)
		return false;
	
	$this->m_current_field_name = $this->GetFieldNameInDataLine( $this->m_current_field_counter++ );
	$this->SetCurrentFieldInfo();
	
	return true;
}

public function PreviousField()
{
	if($this->m_current_field_counter < 1)
		return false;
	
	$this->m_current_field_name = $this->GetFieldNameInDataLine( $this->m_current_field_counter-- );
	$this->SetCurrentFieldInfo();
	
	return true;
}

public function HasRupted()
{
	//wbDebug::Dump($this->m_has_rupted, "wbReportTable", "HasRupted");
	foreach($this->m_has_rupted as $field_name => $has_rupted)
	{
		if($has_rupted == true)
		{
			wbDebug::Message("$field_name has rupted ...");
			return true;
		}
	}
	
	return false;
}

// Informe de l'état des ruptures
private function SetRuptureInfo()
{
	$currentFieldName = $this->GetCurrentFieldName();	

	/*if( $this->IsFieldHavingRupture($currentFieldName) )
		return false;*/
	
	//wbDebug::Message("Setting rupture information of field " . $this->GetCurrentFieldName());
	
	$this->m_last_rupture_value[ $currentFieldName ] =
					$this->m_current_rupture_value[ $currentFieldName ];
	
	$this->m_current_rupture_value[ $currentFieldName ] = $this->GetCurrentValue();
	
	//wbDebug::Dump($this->m_last_rupture_value,"wbReportTable","SetRuptureInfo","Valeurs ligne précédente");
	
	/*if( !empty($this->m_last_rupture_value[ $currentFieldName ]) )
	{*/				
		//wbDebug::Message("Vérifie la différence entre les valeurs courante et dernière du champ $currentFieldName");
		if($this->m_last_rupture_value[ $currentFieldName ]
				!= $this->m_current_rupture_value[ $currentFieldName ])
		{
			$this->m_has_rupted[ $currentFieldName ] = true;
		}
		else
			$this->m_has_rupted[ $currentFieldName ] = false;
	//}
}

private function IsFieldHavingRupture($a_field_name)
{
	if($this->GetFieldByName($a_field_name))
	{	
		//wbDebug::Dump($this->m_ruptures_on_fields, "wbReportTable", "IsFieldHavingRupture");
		if( !empty($this->m_ruptures_on_fields[ $a_field_name ]) )
		//if($this->IsARuptureExisting($a_field_name))
			return true;
		else
			return false;
	} else
		return false;

}

private function IsFieldExisting($a_field_name)
{
	foreach($this->GetSelectFields() as $F)
	{
		if($F->GetFieldName() == $a_field_name)
			return true;
	}
	return false;
}

private function GetFieldByName($a_field_name)
{
	
	try
	{
		// Manque d'efficacité car deux appels à foreach ...
		// Le premier pour la vérification de l'existence
		// Le second pour le retour du champ
		if($this->IsFieldExisting($a_field_name))
		{
			foreach($this->GetSelectFields() as $F)
			{
				if($F->GetFieldName() == $a_field_name)
					return $F;
			}
		}
		else
			throw new Exception("Le champ de sélection '$a_field_name' n'existe pas");
	}
	catch(Exception $e)
	{
		wbError::Raise("wbReportTable", "GetFieldByName", $e->getMessage());
		return false;
	}
}

private function SetCurrentFieldInfo()
{
	foreach($this->m_select_fields as $index => $Field)
	{
		if($Field->GetFieldName() == $this->m_current_field_name)
		{	
			$DataLine = $this->GetCurrentDataLine();

			$this->SetCurrentField($Field); 

			$this->m_current_field_value = $DataLine[ $this->GetCurrentFieldName() ];
			
			// Afficher au bon format numérique en fonction du type
			$fieldType = $Field->GetType();
				
			if($fieldType == wbReportField::TypeInt
					|| $fieldType == wbReportField::TypeDouble)
			{
				$this->m_current_field_value = wbDataFormatter::
														FormatNumber(
																	$this->m_current_field_value
																	, $Field->GetNumFormat());
			}
			

			$this->m_current_field_label = $Field->GetLabel();	
			$this->m_current_field_width = $Field->GetWidth();
			$this->m_current_field_align = $Field->GetAlign();
		}
	}
}

public function GetCurrentSubTotalFieldValue()
{
	return $this->m_current_subtotal_field_value;
}

public function GetCurrentFieldName()
{
	return $this->m_current_field->GetFieldName();
}

private function SetCurrentField($a_Field)
{
	$this->m_current_field = $a_Field;
} 

private function GetFieldNameInDataLine($a_index)
{
	if($a_index >= $this->m_current_fields_count)
		return false;
	
	$DataLine = $this->GetCurrentDataLine();
	
	if( !is_array($DataLine))
		return false; 
	
	$Keys = array_keys($DataLine);
	
	$key = $Keys[ $a_index ];
	
	return $key;
}

private function GetCurrentField()
{
	return $this->m_current_field_counter;
}

private function GetCurrentDataLine()
{
	return $this->GetDataLine($this->m_data_line_counter);
}

private function SetCurrentDataLine($n)
{
	$this->m_current_data_line = &$this->m_data_source[ $this->m_data_line_counter ]; 
}

public function GetDataLine($n)
{
	if($n >= $this->m_data_line_count)
		return false;
	
	$DataSource = $this->GetDataSource();
	
	$this->m_current_fields_count = count($DataSource[ $n ]);
	
	return $DataSource[ $n ];
}

public function GetDataSource()
{
	return $this->m_data_source;
}

public function SetDataSourceFromStatement($a_stmt)
{
	if(!is_bool($a_stmt))
	{
		if(!empty($a_stmt))
			$this->SetDataSource($a_stmt->fetchAll());
	}
	else
	{
		wbError::Raise("wbReportTable", "SetDataSourceFromStatement", "Source de données vide (Requête échouée) ...");
	}
}

public function SetDataSource(Array $A_data_source)
{

	for($i=0; $i < count($A_data_source); $i++)
	{
		$DataLine = &$A_data_source[ $i ];
		wbData::RemoveIntegerKeys($DataLine);
	}
	
	$this->m_data_source = $A_data_source;
	$this->m_data_line_count = count($this->m_data_source);
}

public function AddSQLTable($table_name)
{
	array_push($this->m_tables, $table_name);
}

public function AddSelectField(wbSelectField $sfield)
{
	//$this->AddRupture($sfield->GetFieldName());
	array_push($this->m_select_fields, $sfield);
}

public function AddGroupByField(wbGroupByField $gbfield)
{
	array_push($this->m_groupBy_fields, $gbfield);
}

public function AddWhereField(wbWhereField $wfield)
{
	array_push($this->m_where_fields, $wfield);
}

public function GetSelectFieldByName($a_field_name)
{
	foreach($this->GetSelectFields() as $f)
	{
		if($f->GetFieldName() == $a_field_name)
			return $f;
	}
	
	// Nothing found
	
	return false;
}

public function GetFinalField($a_field_name)
{
	// Modifier la regex pour qu'elle n'accepte que les bons caractères dans
	// le nom du champ : lettres majuscules et minuscules, chiffres, underscore
	
	preg_match('/(\w+)\(`(\w+)`\)/', $a_field_name, $Matches);
	
	foreach($this->GetFinalFields() as $field)
	{
		if($field->GetFieldName() == $Matches[ 2 ]) // Si traite ce nom de champ
		{	
			if($field->GetMethod() == $Matches[ 1 ])
				return $field;
		}
	}
	
	// Nothing found
	
	return false;
}

public function ifContainsFinalField()
{
	if( !empty($this->GetFinalFields()))
		return true;
	else
		return false;
}

public function GetSQLTables()
{
	return $this->m_tables;
}

public function GetWhereFields()
{
	return $this->m_where_fields;
}

public function GetSelectFields()
{
	return $this->m_select_fields;
}

public function GetGroupByFields()
{
	return $this->m_groupBy_fields;
}

public function GetFinalFields()
{
	return $this->m_end_fields;
}

public function GetCurrentValue()
{
	return $this->m_current_field_value;
}

public function GetCurrentLabel()
{
	return $this->m_current_field_label;
}

public function GetCurrentAlign()
{
	return $this->m_current_field_align;
}

public function GetCurrentWidth()
{
	return $this->m_current_field_width;
}

public function GetRuptures()
{
	return $this->m_ruptures_on_fields;
}

private function AddRupture($a_field_name)
{
	$this->m_last_rupture_value[ $a_field_name ] = 0;
	$this->m_current_rupture_value[ $a_field_name ] = 0;
	$this->m_subtotals_on_fields[ $a_field_name ] = False;
}

}